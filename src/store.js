import firebase from 'firebase';
import { createStore } from 'redux';

const initialState = { loggedIn: false, count: 0 };

const store = createStore((state = initialState, action) => {

  switch (action.type) {
    case 'COUNT':
      return { ...state, count: (state.count) + 1 };
    case 'LOGGED_IN': {
      let user = firebase.auth().currentUser;
      console.log('login true action');
      return {...state, loggedIn: true, user: user};
    }
    case 'LOGGED_OUT': {
      console.log('logout action');
      return {...state, loggedIn: false, user: null};
    }
    default:
      return state;
  }
});

export default store;
