import firebase from 'firebase';
import store from './store';
//
//
// let methods = {
//   findUser: function (userId) {
//     return new Promise((resolve, reject) => {
//       var ref = database.ref('users');
//       ref.orderByChild("uid").equalTo('userId').once('child_added', function (snapshot) {
//         if (snapshot.exists()) {
//           resolve(snapshot.val());
//         } else {
//           reject('not found');
//         }
//       });
//     });
//   }
// };

var promise = firebase.auth().getRedirectResult().then(function(result) {
  console.info('getRedirectResult');
  if (result.credential) {
    console.log('getRedirectResult credentials', result.credential);
  }

}, function(err) {
  console.error('google login err', err);
});

firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    store.dispatch({type: 'LOGGED_IN'});
  } else {
    store.dispatch({type: 'LOGGED_OUT'});
  }

});

export default promise;






