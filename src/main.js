import 'babel-polyfill'
import React from 'react'
import {BrowserRouter, Switch, Route, Link} from 'react-router-dom'
import {render} from 'react-dom';
import VehiclePage from './vehicle';
import Vehicle from './vehicle/vehicle';
import './firebase';

const Index = () =>(
  <p>Index</p>
);
const Header = () => (
  <header>
    <nav>
      <ul>
        <li><Link to="/">Index</Link></li>
        <li><Link to="/vehicle">Vehicle</Link></li>
      </ul>
    </nav>
    <h1>Hello world</h1>
  </header>
);
const App = () => (
  <div>
    <Header />
    <Main />
  </div>
);
const Main = () => (
  <main>
    <Switch>
      <Route exact path="/" component={Index}/>
      <Route exact path="/vehicle" component={VehiclePage}/>
      <Route path="/vehicle/:id" component={Vehicle}/>
    </Switch>

  </main>
);

// Finally, we render a <Router> with some <Route>s.
// It does all the fancy routing stuff for us.
render((
  <BrowserRouter>
    <App />
  </BrowserRouter>
), document.getElementById('app'));
