import firebase from 'firebase';

var config = {
  apiKey: "AIzaSyDCtyK-xwI1ovqA3s6NbKhUgEVDYn3J4u8",
  databaseURL: "https://flickering-inferno-3146.firebaseio.com",
  authDomain: "flickering-inferno-3146.firebaseapp.com",
};


firebase.initializeApp(config);
// firebase.database.enableLogging(true);

const database = firebase.database();
export default database;
