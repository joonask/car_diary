import React, { PropTypes } from 'react';
import {Link} from 'react-router-dom';
import { title, html } from './vehicle.md';
import firebase from 'firebase';

class VehiclePage extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    let that = this;
    that.setState({
      loading: true
    });
    this.getVehicles().then(vehicles=>{
      that.setState({
        loading: false,
        vehicles: vehicles
      });
    });
  }

  checkStatus = (e) => {
    console.log('check', this.auth);
    e.preventDefault();
    return false;
  };
  getVehicles = () => {
    return new Promise((resolve, reject) => {
      let ref = firebase.database().ref("vehicle");
      ref.on('value', function (snapshot) {
        let vals = Object.values(snapshot.val());
        resolve(vals);
      }, reject);
    });
  };

  login = (e) => {
    var provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithRedirect(provider);
    e.preventDefault();
    return false;
  };

  logout = (e) => {
    firebase.auth().signOut();
    e.preventDefault();
    return false;
  };

  render() {
    var authButton, displayName;
      if (this.props.store && this.props.store.getState().loggedIn) {
        displayName = this.props.store.getState().user.displayName;
        authButton = <a href onClick={this.logout}>Logout</a>;
      } else {
        authButton = <a href onClick={this.login}>Login</a>;
      }
      if (this.state.loading) {
        return (<p>Loading</p>);
      }

    return (
      <div>
        <h2>Vehicles</h2>
        <ul>
          {this.state.vehicles.map(vehicle => {
              return (<li key={vehicle.id}>
                <Link to={`/vehicle${vehicle.id}`}>{vehicle.make} {vehicle.model}</Link>
              </li>)
          })}
        </ul>
      </div>
    );
  }
}
export default VehiclePage;
