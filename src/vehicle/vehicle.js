import React, { PropTypes } from 'react';
import {Link} from 'react-router-dom';
import firebase from 'firebase';

class Vehicle extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    let that = this;
    this.setState({
      loading: true
    });
    this.getVehicle(this.props.match.params.id).then(vehicle=>{
      that.setState({
        loading: false,
        vehicle: vehicle
      });
    });
  }

  getVehicle = (id) => {
    return new Promise((resolve, reject) => {
      let ref = firebase.database().ref("vehicle/"+ id);
      ref.on('value', function (snapshot) {
        resolve(snapshot.val());
      }, reject);
    });
  };


  render() {
    if (this.state.loading) {
      return (<p>Loading</p>);
    }
    return (
      <div>
        <h4>{this.state.vehicle.make} {this.state.vehicle.model}</h4>
        <div dangerouslySetInnerHTML={{__html: this.state.vehicle.description}}></div>
        <br/>
        <Link to="/vehicle">Back</Link>
      </div>
    );
  }
}

export default Vehicle;
