import React from 'react';

function resolve(routes, context) {
  for (const route of routes) { // eslint-disable-line no-restricted-syntax
    return route.load().then(X => <X route={{ ...route }} error={context.error} />);
  }

  const error = new Error('Kilke not found');
  error.status = 404;
  return Promise.reject(error);
}

export default { resolve };

