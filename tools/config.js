module.exports = {
  title: 'Vehicle diary',        // Your website title
  url: 'https://vehicle.kallio.pw',          // Your website URL
  project: 'react-static-boilerplate',      // Firebase project. See README.md -> How to Deploy
  trackingID: 'UA-XXXXX-Y',                 // Google Analytics Site's ID
};
